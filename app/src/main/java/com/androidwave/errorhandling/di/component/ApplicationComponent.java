package com.androidwave.errorhandling.di.component;

import android.content.Context;

import com.androidwave.errorhandling.WaveApp;
import com.androidwave.errorhandling.di.ApplicationContext;
import com.androidwave.errorhandling.di.module.ApplicationModule;
import com.androidwave.errorhandling.ui.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created on : Jan 19, 2019
 * Author     : AndroidWave
 * Email    : info@androidwave.com
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(WaveApp app);

    void inject(MainActivity service);

    @ApplicationContext
    Context context();


}
