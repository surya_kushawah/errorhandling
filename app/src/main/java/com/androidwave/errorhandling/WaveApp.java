package com.androidwave.errorhandling;

import android.app.Application;

import com.androidwave.errorhandling.di.component.ApplicationComponent;
import com.androidwave.errorhandling.di.component.DaggerApplicationComponent;
import com.androidwave.errorhandling.di.module.ApplicationModule;

/**
 * Created on : Jan 19, 2019
 * Author     : AndroidWave
 * Email    : info@androidwave.com
 */
public class WaveApp extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
