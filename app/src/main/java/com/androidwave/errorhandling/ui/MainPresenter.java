package com.androidwave.errorhandling.ui;


import android.util.Log;

import com.androidwave.errorhandling.network.NetworkService;
import com.androidwave.errorhandling.network.WrapperError;
import com.google.gson.JsonSyntaxException;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

/**
 * Created on : Jan 19, 2019
 * Author     : AndroidWave
 * Email    : info@androidwave.com
 */
public class MainPresenter implements MainMvp.Presenter {
    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;
    private CompositeDisposable mDisposable;
    private NetworkService mService;
    private MainMvp.View mView;
    private static final String TAG = "MainPresenter";

    @Inject
    public MainPresenter(NetworkService service, CompositeDisposable disposable) {
        this.mService = service;
        this.mDisposable = disposable;
    }


    @Override
    public void getUserProfile() {
        if (mView != null) {
            mView.showLoading(true);
        }
        mDisposable.add(
                mService.getUserProfile()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnTerminate(() -> {
                            if (mView != null) {
                                mView.showLoading(false);
                            }
                        })
                        .subscribe(response -> {
                            if (mView != null) {
                                mView.showLoading(false);

                           //     mView.onSuccess(response.getData());
                                /**
                                 * Update view here
                                 */
//                                if (!response.getError()) {
                                    mView.onSuccess(response);
//                                } else {
//                                    mView.onError(response.getMessage());
//                                }
                            }
                        }, error -> {
                            if (mView != null) {
                                mView.showLoading(false);
                                handleApiError(error);
                            }
                        })
        );
    }

    @Override
    public void detachView() {
        mDisposable.clear();
    }

    @Override
    public void attachView(MainMvp.View view) {
        this.mView = view;
    }

    @Override
    public void handleApiError(Throwable error) {
        if (error instanceof HttpException) {
            switch (((HttpException) error).code()) {
                case HttpsURLConnection.HTTP_UNAUTHORIZED:
                    mView.onError("Unauthorised User ");
                    break;
                case HttpsURLConnection.HTTP_FORBIDDEN:
                    mView.onError("Forbidden");
                    break;
                case HttpsURLConnection.HTTP_INTERNAL_ERROR:
                    mView.onError("Internal Server Error");
                    break;
                case HttpsURLConnection.HTTP_BAD_REQUEST:
                    mView.onError("Bad Request");
                    break;
                case API_STATUS_CODE_LOCAL_ERROR:
                    mView.onError("No Internet Connection");
                    break;
                default:
                    mView.onError(error.getLocalizedMessage());

            }
        } else if (error instanceof WrapperError) {
            mView.onError(((WrapperError) error).getMessage());
        } else if (error instanceof JsonSyntaxException) {
            mView.onError("Something Went Wrong API is not responding properly!");
        } else {
            Log.d(TAG, "handleApiError: ");
            mView.onError(error.getMessage());
        }

    }
}
