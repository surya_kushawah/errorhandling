package com.androidwave.errorhandling.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidwave.errorhandling.R;
import com.androidwave.errorhandling.WaveApp;
import com.androidwave.errorhandling.network.pojo.Place;
import com.androidwave.errorhandling.network.pojo.UserProfile;
import com.androidwave.errorhandling.utils.CommonUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainMvp.View {

    //  ActivityComponent mActivityComponent;
    private static final String TAG = "MainActivity";
    @Inject
    MainMvp.Presenter mPresenter;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtDesignation)
    TextView txtDesignation;
    @BindView(R.id.txtFollowers)
    TextView txtFollowers;
    @BindView(R.id.txtFollowing)
    TextView txtFollowing;
    @BindView(R.id.txtUsername)
    TextView txtUsername;
    @BindView(R.id.txtBio)
    TextView txtBio;
    @BindView(R.id.txtPhone)
    TextView txtPhone;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.txtWorkAt)
    TextView txtWorkAt;
    @BindView(R.id.imageViewProfilePic)
    ImageView imageViewProfilePic;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((WaveApp) getApplication()).getComponent().inject(this);
        mPresenter.attachView(this);
        mPresenter.getUserProfile();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showLoading(boolean isLoading) {
        if (isLoading) {
            mDialog = CommonUtils.showLoadingDialog(this);
        } else {
            if (mDialog != null)
                mDialog.dismiss();
        }
    }

    @Override
    public void onSuccess(UserProfile mProfile) {
        txtTitle.setText(String.format("%s %s", mProfile.getFirstName(), mProfile.getLastName()));
        txtDesignation.setText(mProfile.getDesignation());
        txtFollowers.setText(String.valueOf(mProfile.getFollowersCounter()));
        txtFollowing.setText(String.valueOf(mProfile.getFollowingCount()));
        txtUsername.setText(mProfile.getUserName());
        txtPhone.setText(mProfile.getMobileNumber());
        txtWorkAt.setText(mProfile.getWorkAt());
        txtBio.setText(mProfile.getBio());
        Place mPlace = mProfile.getLocation().get(0);
        txtAddress.setText(String.format("%s %s %s", mPlace.getCity(), mPlace.getState(), mPlace.getCountry()));

        Glide.with(MainActivity.this).load(mProfile.getProfilePicUrl()).apply(new RequestOptions().centerCrop().circleCrop().placeholder(R.drawable.profile_pic)).into(imageViewProfilePic);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void onError(String message) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView textView = sbView
                .findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this, R.color.white));
        snackbar.show();
    }
}
