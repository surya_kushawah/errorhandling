package com.androidwave.errorhandling.ui;

import com.androidwave.errorhandling.network.pojo.UserProfile;

/**
 * Created on : Jan 19, 2019
 * Author     : AndroidWave
 * Email    : info@androidwave.com
 */
public class MainMvp {
    interface View {

        void showLoading(boolean isLoading);

        void onSuccess(UserProfile mProfile);

        void onError(String message);
    }

    public interface Presenter {

        void getUserProfile();

        void detachView();

        void attachView(View view);

        void handleApiError(Throwable error);


    }
}
