package com.androidwave.errorhandling.network;

import com.androidwave.errorhandling.network.pojo.UserProfile;
import com.androidwave.errorhandling.network.pojo.WrapperResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created on : Jan 19, 2019
 * Author     : AndroidWave
 * Email    : info@androidwave.com
 */
public interface NetworkService {
    /**
     * @return Observable feed response
     */
    @GET("user.php")
    Observable<UserProfile> getUserProfile();
}
