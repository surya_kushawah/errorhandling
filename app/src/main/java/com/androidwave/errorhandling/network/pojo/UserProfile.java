package com.androidwave.errorhandling.network.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created on : Jan 19, 2019
 * Author     : AndroidWave
 * Email    : info@androidwave.com
 */
public class UserProfile {

    @SerializedName("about")
    private String mAbout;
    @SerializedName("bio")
    private String mBio;
    @SerializedName("channelCount")
    private Long mChannelCount;
    @SerializedName("designation")
    private String mDesignation;
    @SerializedName("firstName")
    private String mFirstName;
    @SerializedName("followersCounter")
    private Long mFollowersCounter;
    @SerializedName("followingCount")
    private Long mFollowingCount;
    @SerializedName("lastName")
    private String mLastName;
    @SerializedName("location")
    private List<Place> mLocation;
    @SerializedName("mobileNumber")
    private String mMobileNumber;
    @SerializedName("profilePicUrl")
    private String mProfilePicUrl;
    @SerializedName("studiedAt")
    private String mStudiedAt;
    @SerializedName("userId")
    private String mUserId;
    @SerializedName("userName")
    private String mUserName;
    @SerializedName("workAt")
    private String mWorkAt;

    public String getAbout() {
        return mAbout;
    }

    public void setAbout(String about) {
        mAbout = about;
    }

    public String getBio() {
        return mBio;
    }

    public void setBio(String bio) {
        mBio = bio;
    }

    public Long getChannelCount() {
        return mChannelCount;
    }

    public void setChannelCount(Long channelCount) {
        mChannelCount = channelCount;
    }

    public String getDesignation() {
        return mDesignation;
    }

    public void setDesignation(String designation) {
        mDesignation = designation;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public Long getFollowersCounter() {
        return mFollowersCounter;
    }

    public void setFollowersCounter(Long followersCounter) {
        mFollowersCounter = followersCounter;
    }

    public Long getFollowingCount() {
        return mFollowingCount;
    }

    public void setFollowingCount(Long followingCount) {
        mFollowingCount = followingCount;
    }


    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public List<Place> getLocation() {
        return mLocation;
    }

    public void setLocation(List<Place> location) {
        mLocation = location;
    }

    public String getMobileNumber() {
        return mMobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        mMobileNumber = mobileNumber;
    }

    public String getProfilePicUrl() {
        return mProfilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        mProfilePicUrl = profilePicUrl;
    }

    public String getStudiedAt() {
        return mStudiedAt;
    }

    public void setStudiedAt(String studiedAt) {
        mStudiedAt = studiedAt;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getWorkAt() {
        return mWorkAt;
    }

    public void setWorkAt(String workAt) {
        mWorkAt = workAt;
    }


}
